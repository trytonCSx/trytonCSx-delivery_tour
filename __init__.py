# Copyright 2021 Hartmut Goebel <h.goebel@crazy-compilers.com>
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.pool import Pool

from . import delivery_tour


def register():
    Pool.register(
        delivery_tour.Station,
        delivery_tour.StationAddressRelation,
        delivery_tour.Van,
        delivery_tour.Tour,
        module='delivery_tour', type_='model')
