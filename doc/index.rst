##############
Country Module
##############

The *Country Module* provides the basic features needed to handle data
related to countries, their subdivisions, and their postal codes.
It also provides some scripts that are used to load standard countries,
subdivisions, and selected countries' postal codes into Tryton.

This module provides a list of `Countries <model-country.country>` and other
reference data which is normally managed using the
:doc:`provided scripts <setup>`.

.. tip::

   From a country you can :guilabel:`Open related records` and select
   :guilabel:`Zip` to find a list of all the `Postal Codes <model-country.zip>`
   for that country.
